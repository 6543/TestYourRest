package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

const constString = "abcdefghijklmnopqrstuvwxyz123456789"

func main() {
	varString := "start value"
	mutex := sync.RWMutex{}

	http.HandleFunc("/const", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = fmt.Fprintf(w, constString)
	})

	http.HandleFunc("/var", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			mutex.RLock()
			defer mutex.RUnlock()
			w.WriteHeader(http.StatusOK)
			_, _ = fmt.Fprintf(w, varString)

		case http.MethodPut:
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}

			mutex.Lock()
			defer mutex.Unlock()
			varString = string(body)
			w.WriteHeader(http.StatusAccepted)

		case http.MethodDelete:
			mutex.Lock()
			defer mutex.Unlock()
			varString = ""
			w.WriteHeader(http.StatusNoContent)

		default:
		}
	})

	http.ListenAndServe("0.0.0.0:8080", nil)
}
